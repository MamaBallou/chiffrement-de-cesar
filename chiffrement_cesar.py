import string

#Fonction de traduction non circulaire
#Paramètre : texte -> la phrase à traduire
#decalage -> la clé de décalage positive ou négative si cryptage ou décryptage
#Retourne : la traduction, <string>
def traduction(texte, decalage) :
    trad = ""
    #Pour chaque lettre on la décale dans la table ascii du nombre de décalage
    for lettre in texte :
        lettre = chr(ord(lettre) + decalage)
        trad += lettre
    return trad

#Fonction de traduction circulaire
#Paramètre : texte -> la phrase à traduire
#decalage -> la clé de décalage positive ou négative si cryptage ou décryptage
#Retourne : la traduction, <string>
def traduction_circulaire(texte, decalage) :
    trad = ""
    #On crée 2 tableau où les lettres sont décaler, ils servent de référence de traduction
    tab_min = string.ascii_lowercase[decalage:]+string.ascii_lowercase[:decalage]
    tab_maj = string.ascii_uppercase[decalage:]+string.ascii_uppercase[:decalage]

    #Pour chaque lettre
    for lettre in texte :
        #Si c'est une lettre minuscule
        if(lettre in string.ascii_lowercase):
            trad+=tab_min[ord(lettre)-ord('a')]
        else:
            #Si c'est une lettre majuscule
            if(lettre in string.ascii_uppercase):
                trad+=tab_maj[int(ord(lettre)-ord('A'))]
            else:
                #Autre caractères
                trad += traduction(lettre, decalage)
                
    return trad
