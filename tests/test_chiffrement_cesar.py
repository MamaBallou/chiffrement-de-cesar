import unittest
from chiffrement_cesar import traduction
from chiffrement_cesar import traduction_circulaire

class TestChiffrementCesarFunctions(unittest.TestCase):
    def test_traduction(self) :
        self.assertEqual(traduction("Hello", 8), "Pmttw")
        self.assertEqual(traduction("Pmttw", -8), "Hello")

    def test_traduction_circulaire(self) :
        self.assertEqual(traduction_circulaire("XyZ", 3), "AbC")
        self.assertEqual(traduction_circulaire("Hello", 8), "Pmttw")
        self.assertEqual(traduction_circulaire("AbC", -3), "XyZ")
        self.assertEqual(traduction_circulaire("Pmttw", -8), "Hello")
        self.assertEqual(traduction_circulaire("hé!", 2), "jë#")