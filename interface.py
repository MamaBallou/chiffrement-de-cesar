from tkinter import *
from chiffrement_cesar import *


DECALAGE = 0

#Fonction exécutée lors du click sur 'crypter'
def click_crypt():
    label_traduction.configure(text=str(value.get()))
    DECALAGE = int(spinbox_clef.get())
    
    cryptage_bouton.grid_forget()
    decryptage_bouton.grid_forget()
    spinbox_clef.grid_forget()
    rb_chiffrement.grid_forget()
    rb_chiffrement_circulaire.grid_forget()

    return_bouton.grid(column=1,row=2)

    if(rb_valeur_coche.get()=='1'):
        value.set(traduction(value.get(), DECALAGE))
    if(rb_valeur_coche.get()=='2'):
        value.set(traduction_circulaire(value.get(), DECALAGE))

#Fonction exécutée lors du click sur 'décrypter'
def click_decrypt():
    label_traduction.configure(text=str(value.get()))
    DECALAGE = int(spinbox_clef.get())
    
    cryptage_bouton.grid_forget()
    decryptage_bouton.grid_forget()
    spinbox_clef.grid_forget()
    rb_chiffrement.grid_forget()
    rb_chiffrement_circulaire.grid_forget()

    return_bouton.grid(column=1,row=2)
    
    if(rb_valeur_coche.get()=='1'):
        value.set(traduction(value.get(), -DECALAGE))
    if(rb_valeur_coche.get()=='2'):
        value.set(traduction_circulaire(value.get(), -DECALAGE))

#Fonction exécutée lors du click sur 'retour'
def click_return():
    label_traduction.configure(text="Saisir votre phrase")
    value.set("")
    cryptage_bouton.grid(column=0, row=2)
    decryptage_bouton.grid(column=2, row=2)
    spinbox_clef.grid(column=1, row=2)
    rb_chiffrement.grid(column=0,row=1)
    rb_chiffrement_circulaire.grid(column=2,row=1)

    return_bouton.grid_forget()

#Initialisation de la fenêtre
frame = Tk()
frame.title("Chiffrement de César")

#Initialisation de la valeur à saisir
value = StringVar() 
value.set("")

#Label demandant de saisir la phrase
label_traduction=Label(frame,text="Saisir votre phrase")
label_traduction.grid(column=1,row=0)

#Pour choisir là clée de décalage de l'alphabet
spinbox_clef = Spinbox(frame, from_=0, to=25)
spinbox_clef.grid(column=1, row=2)

#Là où on entre la phrase
input_phrase = Entry(frame, textvariable=value)
input_phrase.grid(column=1, row=1)

#Bouton qui au click appel la fonction click_crypt
cryptage_bouton=Button(frame, text="Cryptage", command=click_crypt)
cryptage_bouton.grid(column=0,row=2)

#Bouton qui au click appel la fonction click_return
return_bouton=Button(frame, text="Retour", command=click_return)
return_bouton.grid_forget()

#Bouton qui au click appel la fonction click_decrypt
decryptage_bouton=Button(frame, text="Décryptage", command=click_decrypt)
decryptage_bouton.grid(column=2,row=2)

#Case à cocher pour savoir si c'est un chiffrement circulaire ou non
rb_valeur_coche = StringVar() 
rb_chiffrement = Radiobutton(frame, text="chiffrement", variable=rb_valeur_coche, value=1)
rb_chiffrement_circulaire = Radiobutton(frame, text="chiffrement circulaire", variable=rb_valeur_coche, value=2)
rb_chiffrement.grid(column=0,row=1)
rb_chiffrement_circulaire.grid(column=2,row=1)
rb_chiffrement.select()

frame.mainloop()